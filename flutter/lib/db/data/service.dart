import './user.dart';
import './forwarding.dart';

/// 映射 服務
class Service {
  /// 顯示給用戶 的 服務名稱
  String name;

  /// ssh 服務器 地址
  String addr;

  /// 連接用戶
  User user;

  /// 映射的端口
  List<Forwarding> forwarding;
}
