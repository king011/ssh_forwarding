package ssh_forwarding

import (
	"errors"
	"log"
	"net"
	"strings"
	"sync"

	"golang.org/x/crypto/ssh"
)

// Service 端口 映射服務
type Service struct {
	client   *ssh.Client
	listener []net.Listener
	buffer   int
}

// New 創建 映射服務 服務
func New(addr string, cnf *ssh.ClientConfig, buffer int) (srv *Service, e error) {
	
	client, e := ssh.Dial("tcp", addr, cnf)
	if e != nil {
		return
	}
	if buffer < 1024 {
		buffer = 1024
	}
	srv = &Service{
		client: client,
		buffer: buffer,
	}
	return
}

// Close 關閉 服務
func (srv *Service) Close() (e error) {
	if srv.client == nil {
		return
	}
	for i := 0; i < len(srv.listener); i++ {
		e = srv.listener[i].Close()
		if e != nil {
			log.Println(e)
		}
	}
	srv.listener = nil
	e = srv.client.Close()
	srv.client = nil
	return
}

type _Item struct {
	l      net.Listener
	target string
}

// Run 運行 服務
func (srv *Service) Run(m map[string]string) (e error) {
	keys := make(map[string]_Item)
	var l net.Listener
	listener := make([]net.Listener, 0, len(m))
	for k, v := range m {
		k = strings.TrimSpace(k)
		v = strings.TrimSpace(v)
		if _, ok := keys[k]; ok {
			continue
		}
		l, e = net.Listen("tcp", k)
		if e != nil {
			for i := 0; i < len(listener); i++ {
				listener[i].Close()
			}
			return
		}
		listener = append(listener, l)
		keys[k] = _Item{
			l:      l,
			target: v,
		}
	}
	if len(listener) == 0 {
		e = errors.New("listener empty")
		return
	}
	srv.listener = listener

	// 運行
	var wait sync.WaitGroup
	wait.Add(len(listener))
	for _, item := range keys {
		go func(item _Item) {
			srv.run(item.l, item.target)
			wait.Done()
		}(item)
	}
	wait.Wait()
	return
}
func (srv *Service) run(l net.Listener, target string) {
	for {
		c, e := l.Accept()
		if e != nil {
			break
		}
		go srv.bridge(c, target)
	}
}
func (srv *Service) bridge(l net.Conn, target string) {
	defer l.Close()
	r, e := srv.client.Dial("tcp", target)
	if e != nil {
		log.Println(e)
		return
	}

	lb := make([]byte, srv.buffer)
	rb := make([]byte, srv.buffer)
	go func() {
		var n int
		var e error
		for {
			n, e = r.Read(rb)
			if e != nil {
				break
			}
			if n == 0 {
				continue
			}
			_, e = l.Write(rb[:n])
			if e != nil {
				break
			}
		}

		r.Close()
		l.Close()
	}()

	var n int
	for {
		n, e = l.Read(lb)
		if e != nil {
			break
		}
		if n == 0 {
			continue
		}
		_, e = r.Write(lb[:n])
		if e != nil {
			break
		}
	}
	r.Close()
	l.Close()
}
