#/bin/bash
dir=$(cd $(dirname $BASH_SOURCE) && pwd)

if [ ! -d "$dir/bin" ]; then
    mkdir "$dir/bin"
fi

gomobile bind -o "$dir/bin/ssh_forwarding.aar" -target=android gitlab.com/king011/ssh_forwarding
